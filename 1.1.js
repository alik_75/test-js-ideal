//Imperative
function calculatePower5Imperative(nums) {
  const poweredNums = [];
  for (let i = 0; i < nums.length; i++) {
    const result = singlePower(nums[i],5);
    poweredNums.push(result);
  }
  return poweredNums;
}

function singlePower(num,pow){
      const result=Math.pow(num,pow);
      return result;
}


//Declaritive
function calculatePower5Declaritive(nums){
      return nums.map((num) => singlePower(num, 5));
}


// console.log(calculatePower5Imperative([1,2,3,4]));
console.log(calculatePower5Declaritive([1,2,3,4]));
