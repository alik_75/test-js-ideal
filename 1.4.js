const express= require('express');
const app=express();

// You can handle all request before and then
// Handle 404 errors
app.get('*', function(req, res){
      res.status(404).render('pages/404',{title:"Not Found!",hideMenu:true});
});

// Notice: Sorry I didn't understand well, If you mean that check request for having proxy on it,
// you can check the request headers specially for "HOST" property.