const instancePromise=(id)=>{
      return new Promise((resolve,reject)=>{
            setTimeout(()=>{
                  console.log("Promise Called #"+ id);
                  resolve(id);
            },1000*Math.floor(Math.random() * 10));
            
      });
}

async function testPromises(){
      console.log("Main Process Start");
      const P1=await instancePromise(1);
      const P2=instancePromise(2);
      const P3=instancePromise(3);
      const P4=instancePromise(4);
      const P5=instancePromise(5);
      const P6=instancePromise(6);
      const P7=instancePromise(7);

      Promise.all([P4,P5,P6,P7]).then((res)=>{
            console.log("All P4,P5,P6,P7 Resolved");
      }).catch((err)=>{
            console.log("One of Promises has error");
      });
      console.log("Main Process End");

}

testPromises();